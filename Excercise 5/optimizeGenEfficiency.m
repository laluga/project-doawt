function [Nturn_opt, Rload_opt, efficiency_opt] = optimizeGenEfficiency(RPM, desired_torque)

%% Input data is the rpm and the desired torque
% Based on this the number of turns and the load resistance which gives the
% best efficiency is calculated

% Rload = [0.5:50]
% Number of turns = [20, 60]
data = importGenData("C:\Users\lgaje\OneDrive\Dokumente\Uni\01_Norwegen\Uni\Design of a Wind Turbine\Group Work\TEP4175 PMSG Generator Radial Design.xlsx", "RADIAL", [10, 73]); 
Pfriciton = data.Value(44);
Nphase = data.Value(11);
Rwire = data.Value(1);
Wire_length_per_turn = data.Value(33);
Ns = data.Value(6);
m = data.Value(3);
additional_res_in_stator = data.Value(39);
coil_res_temp_correction = data.Value(40);
omega_e = data.Value(46);
N0 = data.Value(19);
Ls0 = data.Value(20);
Bmax = data.Value(16);
Lactive = data.Value(4);
fw = data.Value(5);
Ns = data.Value(6);
dist_min = data.Value(9);
dist_pole = data.Value(10);
fprintf("Used Ls0 for three phase generator!");

efficiency_opt = 0;
Nturn_opt = 0;
Rload_opt = 0;

for Rload = 0.5:0.01:50
    for Nturn = 20:60  
        w = (2*pi*RPM)/60;
        Ls = Ls0*(Nturn/N0)^2;
        Xgen = Ls*omega_e;
        Rtot = Rwire*Wire_length_per_turn*Nturn*Ns/m*additional_res_in_stator*coil_res_temp_correction;        
        vavg = w*(dist_min + dist_pole);
        Eind = 2*Nturn*Bmax*fw*Ns*Lactive*vavg/sqrt(2);
        I = Eind/(sqrt((Rtot + Rload)^2 + Xgen^2));
        Pel = Nphase*Rload*I^2;
        Pin = (Pfriciton + ...
            Nphase*Rtot*I^2 + ...
            Pel);
        
        Tgen = Pin/w;
        if abs(Tgen - desired_torque) < 0.01 
            efficiency = Pel/Pin;
            if efficiency > efficiency_opt
                efficiency_opt = efficiency;
                Nturn_opt = Nturn;
                Rload_opt = Rload;                
            end
        end
    end
end

    fprintf("\nOptimal values:\n");
    fprintf("Rload = %.2f\n", Rload_opt);
    fprintf("Nturn = %.2f\n", Nturn_opt);
    fprintf("Efficiency = %.2f\n", efficiency_opt);

end

