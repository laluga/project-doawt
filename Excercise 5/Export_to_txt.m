function Export_to_txt(N, twist_angle_rad, cord_length, Ri, dr)
    
nullen = zeros(N,1); % due to the 2 extra nodes one at beginning one at the end
BlrelThik = nullen + 0.140381;
BlPAD = nullen + 1/3;
twist_angle_deg = twist_angle_rad*180/pi;
%% First line will be the same as the second one, due to me not knowing how to handle the nodes
T = table(round(Ri,3), nullen, nullen, nullen, round(twist_angle_deg,3), ...
 round(cord_length,3), nullen+1, BlrelThik, BlPAD);

fileID = fopen('Shape_OptimalBlade1.txt', 'w');

fprintf(fileID,'------- ASHES BLADE DEFINITION INPUT FILE -----------------------------------------------------------\n');
fprintf(fileID,'Output format based on AeroDyn v15.03\n');
fprintf(fileID,'====== Airfoil Information ==========================================================================\n');
fprintf(fileID,'Number of airfoils used: %i \n', 1);
fprintf(fileID,'"S826"\n');
fprintf(fileID,'====== Blade Properties =============================================================================\n');
fprintf(fileID,'Number of blade nodes: %i\n', N);
fprintf(fileID,'BlSpn   BlCrvAC BlSwpAC BlCrvAng    BlTwist BlChord BlAFID  BlRelThick  BlPAD\n');
fprintf(fileID, '(m)    (m) (m) (deg)   (deg)   (m) (-) (-) (-)\n');
fclose(fileID);

fileID2 = fopen('Shape_OptimalBlade2.txt', 'w');
writetable(T, 'Shape_OptimalBlade2.txt', 'WriteVariableNames',0, 'Delimiter', 'tab');
fclose(fileID2);

save_path = "Blade_";
t = datetime;
t = char(t);
helper1 = t(13:14);
helper2 = t(16:17);
ending = ".txt";
save_path = save_path + t(1:11) + "_" + helper1 + "_" + helper2 + ending;
copy_handling = 'copy Shape_OptimalBlade1.txt+Shape_OptimalBlade2.txt ' + save_path;
copy_handling = convertStringsToChars(copy_handling);
system(copy_handling)

end