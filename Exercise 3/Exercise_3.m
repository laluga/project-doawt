%% Excerice 3: Design of a Wind Turbine
% Author: Lutz Gajewski
% Date: 11.09.2019
% Using the BEM method to evaluate a wind turbine.

function [C_p, P_rotor, F_t] = Exercise_3()

%% Given data:
D = 0.9;
V = 12;
TSR = 7.5; % tip speed ratio
z = 2; % number of blades
roh = 1.2;
% Wing profile: NREL S826

%% Import data of the airfoil NRELS826:
Airfoildata = importfile("C:\Users\lgaje\OneDrive\Dokumente\Uni\01_Norwegen\Uni\Design of a Wind Turbine\�bung\�bung3\Airfoil data - NREL_S826.txt", [10, inf]);
Re = 100000;

%% Airfoil definition
% Calculation of Excercise 1:
N = 1;
[cord_length, twist_angle, Rm, dr] = getValuesExc1(N);

%% Application of the algorithm for each blade element
% 1) Initial values:
i = 1;
a_i = ones(N,1)*1/3;
a_i_dash = zeros(N,1);
% Three dimensional matrix containing the a and a_dash values for each iteration.
% Each dimension is one iteration step
matrix_a(:,:,1) = [a_i a_i_dash]; 

%% Desired Cp
C_pD = 0.45;
w = TSR*V*2/D;
periphel_velocity = w.*Rm;

while 1 
    % 2,3) Calcultion of flow angle phi and angle_of_attack:
    a_i = matrix_a(:,1,i);
    a_i_dash = matrix_a(:,2,i);

    phi = atan(((1-a_i)*V)./((1+a_i_dash).*periphel_velocity));
    angle_of_attack = phi - twist_angle; % [rad] 
    angle_of_attack_deg = angle_of_attack*180/pi; % [deg]
    
    % 4) Find angle of attack in the look up table, use linear interpolation for
    % not existing values.
    Cl = zeros(N,1); 
    Cd = zeros(N,1);    
    for k = 1:N
        Cl(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cl, angle_of_attack_deg(k));
        Cd(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cd, angle_of_attack_deg(k));
    end
    
    % 5) Calculate Ca and Cr:
    Ca = Cl.*cos(phi) + Cd.*sin(phi);
    Cr = Cl.*sin(phi) - Cd.*cos(phi);

    % 6) Calculate the induction factors:
    F = 2/pi*acos(exp(-(z/2)*((D/2-Rm)./(Rm.*sin(phi)))));
    sigma = (z*cord_length)./(2*pi*Rm);
    K = (4*F.*sin(phi).^2)./(Ca.*sigma);
    a_c = 0.2;
    bool_a_i = a_i < 0.2;
    % Here the script works with the boolean operator bool_a_i. This way
    % you do not need if and else. 
    a_new = bool_a_i./(K+1) + ... % ai < 0.2
        0.5.*~bool_a_i.*(2+K*(1-2*a_c) - sqrt((K*(1-2*a_c)+2).^2 + 4*(K*a_c.^2-1))); % ai >= 0.2                
    a_new_dash = 1./((4.*F.*sin(phi).*cos(phi)./(Cr.*sigma)-1));
    
    % 7) Check for convergence, true: exit while loop, otherwise continue
    i = i + 1;
    matrix_a(:,:,i) = [a_new a_new_dash];
    bool_convergence_a_i = (abs(matrix_a(:,1,i)- matrix_a(:,1,i-1)) <= 10^-6);
    bool_convergence_a_i_dash = (abs(matrix_a(:,2,i)- matrix_a(:,2,i-1)) <= 10^-6);
    
    % Convergence only when both boolean variables are true:
    bool_convergence = bool_convergence_a_i.*bool_convergence_a_i_dash;
    if any(bool_convergence)
        % 8) Calculate the blade element parameter
        B_ep = bool_convergence.*a_i./(1+a_i_dash).*4.*sin(phi);
        % 9) Calculate the cord length
        cord_length = bool_convergence.*B_ep.*(2*pi*V)./(z*Cl*w) + ~bool_convergence.*cord_length; % overwrite the last cord_length
        % 10) Calculate the forces
        dT = z.*Ca*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).^2.*F.*dr;
        dM = z.*Cr*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).*(periphel_velocity.*(1+a_i_dash)./cos(phi)).*F.*dr;
        % 11) Calculate the blade element parameter
        M = sum(dM.*Rm);
        P_rotor = M*w;
        A_rotor = pi*D^2/4;
        C_p = 2*P_rotor/(roh*V^3*A_rotor);
        % 12) Test for convergence:
        if C_p > C_pD
            break % otherwise continue at while 1
        else
            % only change the twist angle when there is convergence in a_i and
            % a_i_dash. Otherwise do not change it.
            twist_angle = bool_convergence.*twist_angle-0.001 + ~bool_convergence.*twist_angle; 
        end
    end
end

figure()
plot(Rm/(D/2), cord_length, 'DisplayName','Cord length - initial', 'Color',[0.850980401039124 0.325490206480026 0.0980392172932625]);
legend()
figure();
hold on;
plot(Rm/(D/2), dM, 'DisplayName','Torque');
legend();
plot(Rm/(D/2), dT, 'DisplayName','Thrust');
legend()        
hold off;
end