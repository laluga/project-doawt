clear all;
clc;
close all;
%% given Values

D = 0.9; %in [m]
U = 12; %in[m/s]
TSR = 7.5; %in[]
z = 2; %number of blades
rho = 1.225; %air density [kg/m^3]
cp = 0.5; %power factor

a = 0;
a_ = 0; %underlinded for a�
R = 0.45; %in [m]
InnerR = 0.045; %radius of first element [m]%number of blade elements
n = 20; %number of blade elements

%% programm

deltaR = (R-InnerR)/n; %radial width of each element
r = (InnerR + deltaR/2:deltaR:R); %vector of radial distance to center element

w = (TSR*U)/R; %Angular velocity [rad/s] 
rpm = 9.54929658551369*w; %convert [rad/s] in [rpm]

K = xlsread('Airfoil data - NREL_S826','Airfoil data - NREL_S826','A10:C102');

C_D = K(:,3);
C_L = K(:,2);
AoA = K(:,1);

a_optimal = AoA(C_L./C_D==max(C_L./C_D));
C_D0=C_D(AoA==a_optimal); %first lift coefficient
C_L0=C_L(AoA==a_optimal); %first drag coefficient

deltaA = pi*(((r+deltaR/2).^2 - (r-deltaR/2).^2))/z; %Calculation of the flow area

deltaP = (1/2)*rho*U.^3*deltaA*cp; %power output per element

deltaT = deltaP/w; %value for torque per element

deltaFT = deltaT./r; %force in torque direction per element

wr = w*r; %peripheral velocity per element

RFV = sqrt(wr.^2+U.^2); %relative flow velocity per element

IFA = (360/(2*pi))*(atan(U./wr)); %relative flow velocity per element

Lc =(2*deltaFT)./(rho*deltaR*RFV.^2.*(C_L0*sin(((2*pi)/360)*(IFA))-C_D0*cos(((2*pi)/360)*(IFA))));%initial cord length per element

%% converting values into vectors (radian in degree)
phi_bl(n)= zeros;
ao_bl(n)= zeros;
C_d_bl(n)= zeros;
C_l_bl(n)= zeros;
Ca_bl(n)=zeros;
Cr_bl(n)=zeros;
a_bl(n)=zeros;
a__bl(n)=zeros;
F_bl(n)=zeros;
K__bl(n)= zeros;

%% BEM iteration

ai = 1e-4;%very low value
for i=1:n %need for all blade elements
    
    ac = 0.2; %given value
    itera = 0; %the counter for iterations
    
    %axial(a0) and rotational (a_) induction factors for the loop
    a0 = 1;
    a_0 = 1;
    % Guess axial and rotational induction factors
    a = 0;
    a_ = 0;
    
    %The convergence criterion must be fulfilled. It must be generated as many loops, the predetermined criterion is achieved
    while abs(a0-a)>ai || abs(a_0-a_)>ai
        itera = itera+1;
        if itera==1000
            error('Iteration does NOT converge! (r=%.3f)',r)
        end

        phi = atan((1-a)*U/((1+a_)*w*r(i))); %2.Calculate flow angle

        theta(i)= (360/(2*pi))*(phi)-a_optimal;
        ao = (360/(2*pi))*(phi)- theta(i); %calculate angle off attack

        %Interpolation for ao
        C_d = interp1(AoA, C_D, ao);
        C_l = interp1(AoA, C_L, ao);

        Ca = C_l*cos(phi)+ C_d*sin(phi); %calculate Ca
        Cr = C_l*sin(phi)- C_d*cos(phi); %calculate Cr

        %befor calculation the induction factors, the parameter for K and F is
        %needed:
        F = 2/pi*acos(exp(-z*(R-r(i))/(2*r(i)*sin(phi))));
        o = z.*Lc./(2*pi*r); %sigma (o)for calculating K
        K_ = 4*F*sin(phi)^2/(Ca*o(i));

        %new induction factors under vertain circumstances
        a0 = a;
        a_0 = a_;
        if a<ac
            a = 1/(K_+1);
        else
            a = 0.5*(2+K_*(1-2*ac)-sqrt((K_*(1-2*ac)+2)^2+4*(K_*ac^2-1)));
        end

        a_ = 1/(4*F*sin(phi)*cos(phi)/(Cr*o(i))-1);
    
    end

    %converting values into vectors (radian in degree)
    phi_bl(i)= 180/pi*phi;
    ao_bl(i)= ao;
    C_d_bl(i)= C_d;
    C_l_bl(i)= C_l;
    Ca_bl(i)= Ca;
    Cr_bl(i)= Cr;
    a_bl(i)= a;
    a__bl(i)= a_;
    F_bl(i)= F;
    K__bl(i)= K_;
end
%10.Calculate the forces:
deltaT = z.*Ca_bl*0.5*rho.*Lc.*(U^2.*(1-a_bl).^2./sin(((2*pi)/360).*(phi_bl)).^2).*F_bl*deltaR;
deltaM = z.*Cr_bl*0.5*rho.*Lc.*(U.*(1-a_bl)./sin(((2*pi)/360)*(phi_bl))).*(w.*r.*((1+a__bl)./cos(((2*pi)/360).*(phi_bl)))).*F_bl*deltaR;

%11.Calculate the torque and thrust
M = sum(deltaM.*r);
T = sum(deltaT);

P = M*o; %Power of the wind turbine
cp_= P/(0.5*rho*U^3*pi*R^2);


%Results for the programm. Output avlues are:
%1.Maximum power coefficient Cp[-]
%2.Maximum power output P[W]
%3.Thrust force FT [N]
%4.Plot showing the torque and thrust distribution versus radius
figure(1)
plot(r/R,deltaT,r/R,deltaM)
xlabel('r/R')
legend('thrust','torque')
%5.Plot showing the cord length versus radius
figure(2)
plot(r/R,Lc)
xlabel('r/R')
legend('chord length')




