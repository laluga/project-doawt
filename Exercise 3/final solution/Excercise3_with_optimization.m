% Author: Lutz Gajewski
% Date: 11.09.2019
% Using the BEM method to evaluate a wind turbine.

function [C_p, P_rotor, T] = Excercise3_2209()

%% Given data:
D = 0.9;
V = 12;
TSR = 7.5; % tip speed ratio
z = 2; % number of blades
roh = 1.2;
% Wing profile: NREL S826

%% Import data of the airfoil NRELS826:
Airfoildata = importfile("C:\Users\lgaje\OneDrive\Dokumente\Uni\01_Norwegen\Uni\Design of a Wind Turbine\�bung\�bung3\Airfoil data - NREL_S826.txt", [10, inf]);

%% Number of airfoils and convergence intervall
N = 20;
epsilon = 10^-6;
C_p = 0;

%% Function for getting the input data with a variable number of foils
[cord_length, ~, Rm, dr] = getValuesExc1(N);
cord_length_initial = cord_length;

%% Application of the algorithm for each blade element
% 1) Initial values:
i = 1;
a_i = ones(N,1)*1/3;
a_i_dash = zeros(N,1);
% Three dimensional matrix containing the a and a_dash values for each iteration.
% Each dimension is one iteration step
matrix_a(:,:,1) = [a_i a_i_dash]; 

C_d = Airfoildata.Cd(:);
C_l = Airfoildata.Cl(:);
AoA = Airfoildata.Angles_of_attack(:);

w = TSR*V*2/D;
periphel_velocity = w.*Rm;

alpha_optimal = ones(N,1).*AoA(C_l./C_d==max(C_l./C_d));
alpha_opt_rad = alpha_optimal*pi/180;

while 1 
    % 2,3) Calcultion of flow angle phi and angle_of_attack:
    a_i = matrix_a(:,1,i);
    a_i_dash = matrix_a(:,2,i);

    argument_atan = ((1-a_i)*V)./((1+a_i_dash).*periphel_velocity);
    phi = atan(argument_atan); %[rad]
    % Atan is defined for - infinite to + infinite
    % output - oi/2 to pi/2 [rad]
    
    % Calculating angle of attack which can be negative!
    twist_angle_rad = phi - alpha_opt_rad; % [rad]    
    
    % 4) Find angle of attack in the look up table, use linear interpolation for
    % not existing values.
    Cl = zeros(N,1); 
    Cd = zeros(N,1);    
    for k = 1:N
        Cl(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cl, alpha_optimal(k));
        Cd(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cd, alpha_optimal(k));
    end
    
    % 5) Calculate Ca and Cr:
    Ca = Cl.*cos(phi) + Cd.*sin(phi);
    Cr = Cl.*sin(phi) - Cd.*cos(phi);

    % 6) Calculate the induction factors:
    argument_acos = exp(-(z/2)*((D/2-Rm)./(Rm.*sin(phi))));
    F = 2/pi*acos(argument_acos);
    
    % Acos ist nur auf -1 bis 1 definiert und wird sonst komplex
    % Output is [0:pi]

    sigma = (z*cord_length)./(2*pi*Rm);
    K = (4*F.*sin(phi).^2)./(Ca.*sigma);
    a_c = 0.2;
    bool_a_i = a_i < 0.2;
    % Here the script works with the boolean operator bool_a_i. This way
    % you do not need if and else. 
    a_new = bool_a_i./(K+1) + ... % ai < 0.2
        0.5.*~bool_a_i.*(2+K*(1-2*a_c) - sqrt((K*(1-2*a_c)+2).^2 + 4*(K*a_c.^2-1))); % ai >= 0.2                
    a_new_dash = 1./((4.*F.*sin(phi).*cos(phi)./(Cr.*sigma)-1));
    
    % 7) Check for convergence, true: exit while loop, otherwise continue
    i = i + 1;
    matrix_a(:,:,i) = [a_new a_new_dash];
    bool_convergence_a_i = abs(matrix_a(:,1,i)- matrix_a(:,1,i-1)) <= epsilon;
    bool_convergence_a_i_dash = abs(matrix_a(:,2,i)- matrix_a(:,2,i-1)) <= epsilon;
    
    % Convergence only when both boolean variables are true:
    bool_convergence = bool_convergence_a_i.*bool_convergence_a_i_dash;
    
    if bool_convergence
        
        % 10) Calculate the forces
        dT = z.*Ca*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).^2.*F.*dr;
        dM = z.*Cr*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).*(periphel_velocity.*(1+a_i_dash)./cos(phi)).*F.*dr;
        % 11) Calculate the blade element parameter
        M = sum(dM.*Rm);
        P_rotor = M*w;
        A_rotor = pi*D^2/4;
        C_p = [C_p; 2*P_rotor/(roh*V^3*A_rotor)];
        deltaC_p = C_p(end) - C_p(end-1);
        
        if deltaC_p < epsilon
            C_p = C_p(end)
            P_rotor = P_rotor
            T = sum(dT)
            figure()
            hold on;
            plot(Rm/(D/2), cord_length_initial, 'DisplayName','Cord length - inital');
            legend()
            plot(Rm/(D/2), cord_length, 'DisplayName','Cord length - after simulation');
            hold off;
            figure();            
            plot(Rm/(D/2), dM, 'DisplayName','Torque');
            hold on;
            legend();
            plot(Rm/(D/2), dT, 'DisplayName','Thrust');
            legend()        
            hold off;
            break;
        else
            Bep = a_new./(1+a_new_dash).*4.*sin(phi);
            cord_length = Bep.*(2*pi*V)./(z.*Cl.*w);
        end
    end
end
end