%% In the first excercise a wind turbine blade will be calculated. 
% All the relevant formulas are given in the lecture. 

%% Given values:
D = 0.9; % diameter [m]
V = 12; % wind velocity [m/s]
TSR = 7.5; % tip speed ratio [-]
C_p = 0.5; % power factor [-]
z = 2; % number of blades [-]
alpha = 6; % anlge of attack[�]
CL = 1.18; % Lift coefficient [-]  
CD = 0.03; % Drag coefficient [-] 
DH = 0.09; % Hub coefficent [m]
roh = 1.2; % Density [kg/m^3]

%% Task: Divide the turbine blade in 10 elements, where each element is
% 0,0405 m, and calculate the following for each element:
% 1. Power output dP
% 2. Velocity triangle
% 3. Force FT
% 4. Twist angle theta
% 5. Cord length LChord

%% 1) Dividing the blade in 10 elements each being 0.0405m long
Ri1 = 0.045;
dr = 0.0405;
Ri = zeros(10,1);
Ri = Ri1 + (find(Ri == 0)-1).*dr;
Ro = Ri + dr;

% 2) calculate mean radius
Rm = Ri + (Ro - Ri)/2;

% 3) Calculate the area dA [m^2]
dA = pi/z*((Ri+dr).^2-Ri.^2);

% 4) Calculate the power [W]
dP = 0.5*roh.*dA*V^3*C_p;

% 5) Calculate the torque [Nm]
w = TSR*V*2/D;
dM = dP./w;

% 6) Calculate the force in torque direction [N]
dM_dash = dP./(w.*Rm);

% 7) periphel velocity [m/2]
Urel = w.*Rm; 

% 8) Calculate the relative velocity [m/2]
W = sqrt(Urel.^2+V^2);

% 9) Calculate the angle of attack + twist angle in rad and degree
phi_rad = asin(V./W);
phi_deg = phi_rad*180/pi;

% 10) Calculate the twist angle [�]
twist_angle = phi_deg - alpha;

% 11) Calculate the cord length [m]
cord_length = 2*dM_dash./(roh*dr.*W.^2.*(CL*sin(phi_rad) - CD*cos(phi_rad)));

% 12) Calculate the Reynolds Number [-]:
v_air = 17.1*10^-6; % [Pa*s]source Wikipedia.
Re = (W.*cord_length)/v_air;

number_of_element = (1:10)';

resultMatrix = [number_of_element, Ri, Ro, Rm, dA, dP, dM, dM_dash, Urel, W, phi_rad, phi_deg, twist_angle, cord_length, Re];
resultTable = array2table(resultMatrix);

resultTable.Properties.VariableNames{1} = 'ElementNumber';
resultTable.Properties.VariableNames{2} = 'InnerRadius';
resultTable.Properties.VariableNames{3} = 'OuterRadius';
resultTable.Properties.VariableNames{4} = 'MeanRadius';
resultTable.Properties.VariableNames{5} = 'Area';
resultTable.Properties.VariableNames{6} = 'Power';
resultTable.Properties.VariableNames{7} = 'Torque';
resultTable.Properties.VariableNames{8} = 'Force_in_Torque_Direction';
resultTable.Properties.VariableNames{9} = 'Peripheral_velocity';
resultTable.Properties.VariableNames{10} = 'RelativeVelocity';
resultTable.Properties.VariableNames{11} = 'TwistAngle_plus_AngleOfAttack';
resultTable.Properties.VariableNames{11} = 'TwistAngle_plus_AngleOfAttack_rad';
resultTable.Properties.VariableNames{12} = 'TwistAngle_plus_AngleOfAttack_deg';
resultTable.Properties.VariableNames{13} = 'Twist_Angle';
resultTable.Properties.VariableNames{14} = 'Cord_Length';
resultTable.Properties.VariableNames{15} = 'Reynolds_Number';

%% Plotting the velcoity triangles
help = zeros(size(W));
help(:) = V;
zero10 = zeros(size(help))';
x = [zero10, Urel', Urel'];
y = [zero10, zero10, help'];

triangle_data = zeros(10,3);
triangle_data(1:10,1) = (1:10)';
triangle_data(1:10,2) = 10 + triangle_data(1:10,1);
triangle_data(1:10,3) = 20 + triangle_data(1:10,1);
triplot(triangle_data,x,y);
% Create ylabel
ylabel('Wind velocity V');
% Create xlabel
xlabel('Periphel velocity w*r');
% Create title
title({'Velocity triangle of each element','Element 1 is the triangle with the smallest area. The next triangle is the next element. '});

plot(Rm/0.45, cord_length);

