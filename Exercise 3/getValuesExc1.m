function [cord_length, twist_angle_rad, Rm, dr] = getValuesExc1(N)
    %% Given values:
    D = 0.9; % diameter [m]
    V = 12; % wind velocity [m/s]
    TSR = 7.5; % tip speed ratio [-]
    C_p = 0.5; % power factor [-]
    z = 2; % number of blades [-]
    alpha = 6; % anlge of attack[�]
    CL = 1.18; % Lift coefficient [-]  
    CD = 0.03; % Drag coefficient [-] 
    DH = 0.09; % Hub coefficent [m]
    roh = 1.2; % Density [kg/m^3]

    %% Task: Divide the turbine blade in 10 elements, where each element is
    % 0,0405 m, and calculate the following for each element:
    % 1. Power output dP
    % 2. Velocity triangle
    % 3. Force FT
    % 4. Twist angle theta
    % 5. Cord length LChord

    %% 1) Dividing the blade in 10 elements each being 0.0405m long
    Ri1 = 0.045;
    % Length of one element
    dr = (D/2-Ri1)/N;
    Ri = zeros(N,1);
    Ri = Ri1 + (find(Ri == 0)-1).*dr;
    Ro = Ri + dr;

    % 2) calculate mean radius
    Rm = Ri + (Ro - Ri)/2;

    % 3) Calculate the area dA [m^2]
    dA = pi/z*((Ri+dr).^2-Ri.^2);

    % 4) Calculate the power [W]
    dP = 0.5*roh.*dA*V^3*C_p;

    % 5) Calculate the torque [Nm]
    w = TSR*V*2/D;
    dM = dP./w;

    % 6) Calculate the force in torque direction [N]
    dM_dash = dP./(w.*Rm);

    % 7) periphel velocity [m/2]
    Urel = w.*Rm; 

    % 8) Calculate the relative velocity [m/2]
    W = sqrt(Urel.^2+V^2);

    % 9) Calculate the angle of attack + twist angle in rad and degree
    phi_rad = asin(V./W);
    phi_deg = phi_rad*180/pi;

    % 10) Calculate the twist angle [�]
    twist_angle = phi_deg - alpha;
    twist_angle_rad = twist_angle*pi/180;

    % 11) Calculate the cord length [m]
    cord_length = 2*dM_dash./(roh*dr.*W.^2.*(CL*sin(phi_rad) - CD*cos(phi_rad)));

    

end

