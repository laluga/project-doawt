% Author: Lutz Gajewski
% Date: 11.09.2019
% Using the BEM method to evaluate a wind turbine.

function [C_p, P_rotor, F_t] = Exercise_3_without_optimization()

%% Given data:
D = 0.9;
V = 12;
TSR = 7.5; % tip speed ratio
z = 2; % number of blades
roh = 1.2;
% Wing profile: NREL S826

%% Import data of the airfoil NRELS826:
Airfoildata = importfile("C:\Users\lgaje\OneDrive\Dokumente\Uni\01_Norwegen\Uni\Design of a Wind Turbine\�bung\�bung3\Airfoil data - NREL_S826.txt", [10, inf]);
Re = 100000;

%% Number of airfoils and convergence intervall
N = 1;
epsilon = 10^-6;

%% Airfoil definition
% Calculation of Excercise 1:
[cord_length, twist_angle_rad, Rm, dr] = getValuesExc1(N);
w = TSR*V*2/D;
periphel_velocity = w.*Rm;

%% Application of the algorithm for each blade element
% 1) Initial values:
i = 1;
a_i = ones(N,1)*1/3;
a_i_dash = zeros(N,1);
% Three dimensional matrix containing the a and a_dash values for each iteration.
% Each dimension is one iteration step
matrix_a(:,:,1) = [a_i a_i_dash]; 

while 1 
    % 2,3) Calcultion of flow angle phi and angle_of_attack:
    a_i = matrix_a(:,1,i);
    a_i_dash = matrix_a(:,2,i);
    
    % 2) 
    phi = atan(((1-a_i).*V)./((1+a_i_dash).*periphel_velocity));
    % 3)
    angle_of_attack = phi - twist_angle_rad;
    angle_of_attack_deg = angle_of_attack*180/pi;
    % 4) Find angle of attack in the look up table, use linear interpolation for
    % not existing values.
    Cl = zeros(N,1); 
    Cd = zeros(N,1);    
    for k = 1:N
        Cl(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cl, angle_of_attack_deg(k));
        Cd(k) = interp1(Airfoildata.Angles_of_attack(:), Airfoildata.Cd, angle_of_attack_deg(k));
    end
    
    Ca = Cl.*cos(phi) + Cd.*sin(phi);
    Cr = Cl.*sin(phi) - Cd.*cos(phi);
    
    a_c = 0.2;
    sigma = z.*cord_length./(2*pi.*Rm);
    argument_acos = exp(-(z/2).*(D/2-Rm)./(Rm.*sin(phi)));
    F = (2.*acos(argument_acos))./(pi);
    K = (4*F.*sin(phi).^2)./(sigma.*Ca);
    
    bool_axial_factor = a_i < 0.2;
    a_i_new = bool_axial_factor./(((4.*F.*sin(phi).^2)./(sigma.*Ca))+1) + ...
        ~bool_axial_factor.*0.5.*(2+K.*(1-2*a_c)-sqrt((K.*(1-2*a_c)+2).^2+4*(K.*a_c^2-1)));
    a_i_dash_new = 1./(((4.*F.*cos(phi).*sin(phi))./(sigma.*Cr))-1);
    
    i = i + 1;
    matrix_a(:,1,i) = a_i_new;
    matrix_a(:,2,i) = a_i_dash_new;
    bool1 = abs(a_i_new - matrix_a(:,1,i-1)) < epsilon;
    bool2 = abs(a_i_dash_new - matrix_a(:,2,i-1)) < epsilon;
    bool_convergence = bool1.*bool2;
    
    if bool_convergence
        
        % 10) Calculate the forces
        dT = z.*Ca*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).^2.*F.*dr;
        dM = z.*Cr*0.5*roh.*cord_length.*(V*(1-a_i)./sin(phi)).*(periphel_velocity.*(1+a_i_dash)./cos(phi)).*F.*dr;
        % 11) Calculate the blade element parameter
        M = sum(dM.*Rm);
        P_rotor = M*w;
        A_rotor = pi*D^2/4;
        C_p = 2*P_rotor/(roh*V^3*A_rotor);
        
        figure()
        plot(Rm/(D/2), cord_length, 'DisplayName','Cord length - initial', 'Color',[0.850980401039124 0.325490206480026 0.0980392172932625]);
        legend()
        figure();
        hold on;
        plot(Rm/(D/2), dM, 'DisplayName','Torque');
        legend();
        plot(Rm/(D/2), dT, 'DisplayName','Thrust');
        legend()        
        hold off;
        break
    end
end